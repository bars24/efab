<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Dashboard</h3>
    </div>
    <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item active">Dashboard</li>
        </ol>
    </div>
    <div>
        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <!-- Row -->
    <div class="card-group">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <h2 class="m-b-0"><i class="fa fa-briefcase text-info"></i></h2>
                        <h3 class="">2456 Jobs</h3>
                        <h6 class="card-subtitle">Posted this week</h6></div>
                    <div class="col-12">
                        <div class="progress">
                            <div class="progress-bar bg-info" role="progressbar" style="width: 85%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Column -->
        <!-- Column -->
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <h2 class="m-b-0"><i class="mdi mdi-alert-circle text-success"></i></h2>
                        <h3 class="">546 Fabricators</h3>
                        <h6 class="card-subtitle">Registered this week</h6></div>
                    <div class="col-12">
                        <div class="progress">
                            <div class="progress-bar bg-success" role="progressbar" style="width: 40%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Column -->
        <!-- Column -->
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <h2 class="m-b-0"><i class="mdi mdi-wallet text-purple"></i></h2>
                        <h3 class="">1024 Experts</h3>
                        <h6 class="card-subtitle">Registered this week</h6></div>
                    <div class="col-12">
                        <div class="progress">
                            <div class="progress-bar bg-primary" role="progressbar" style="width: 56%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Column -->
        <!-- Column -->
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <h2 class="m-b-0"><i class="mdi mdi-buffer text-warning"></i></h2>
                        <h3 class="">$30010</h3>
                        <h6 class="card-subtitle">Total Earnings</h6></div>
                    <div class="col-12">
                        <div class="progress">
                            <div class="progress-bar bg-warning" role="progressbar" style="width: 26%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Recent Job Posts</h4>
                    <div class="table-responsive">
                        <table class="table table-hover earning-box">
                            <thead>
                                <tr>
                                    <th>Job</th>
                                    <th>Bidding Type</th>
                                    <th>Job Expiration</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <h6>Web Developer</h6>
                                    </td>
                                    <td><span class="label label-success">e-fab User</span></td>
                                    <td>Jun. 7, 2017</td>
                                </tr>
                                <tr class="active">
                                    <td>
                                        <h6>Web Developer</h6>
                                    </td>
                                    <td><span class="label label-info">e-fab User</span></td>
                                    <td>Jun. 7, 2017</td>
                                </tr>
                                <tr>
                                    <td>
                                        <h6>Web Developer</h6>
                                    </td>
                                    <td><span class="label label-primary">e-fab User</span></td>
                                    <td>Jun. 7, 2017</td>
                                </tr>
                                <tr>
                                    <td>
                                        <h6>Web Developer</h6>
                                    </td>
                                    <td><span class="label label-danger">e-fab User</span></td>
                                    <td>Jun. 7, 2017</td>
                                </tr>
                                <tr>
                                    <td>
                                        <h6>Web Developer</h6>
                                    </td>
                                    <td><span class="label label-warning">e-fab User</span></td>
                                    <td>Jun. 7, 2017</td>
                                </tr>
                                <tr>
                                    <td>
                                        <h6>Web Developer</h6>
                                    </td>
                                    <td><span class="label label-info">e-fab User</span></td>
                                    <td>Jun. 7, 2017</td>
                                </tr>
                                <tr>
                                    <td>
                                        <h6>Web Developer</h6>
                                    </td>
                                    <td><span class="label label-warning">e-fab User</span></td>
                                    <td>Jun. 7, 2017</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>  
                <div class="card-body">
                    
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <!-- Column -->
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Recent Fabricators</h4>
                    <div class="table-responsive">
                        <table class="table table-hover earning-box">
                            <thead>
                                <tr>
                                    <th colspan="2">Name</th>
                                    <th>Job Posted</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td style="width:50px;"><span class="round"><img src="../assets/images/users/8.jpg" alt="user" width="50"></span></td>
                                    <td>
                                        <h6>Sunil Joshi</h6><small class="text-muted">Burton</small>
                                    </td>
                                    <td>3</td>
                                </tr>
                                <tr class="active">
                                    <td><span class="round"><img src="../assets/images/users/2.jpg" alt="user" width="50"></span></td>
                                    <td>
                                        <h6>Andrew</h6><small class="text-muted">Burton</small>
                                    </td>
                                    <td>23</td>
                                </tr>
                                <tr>
                                    <td><span class="round round-success"><img src="../assets/images/users/1.jpg" alt="user" width="50"></span></td>
                                    <td>
                                        <h6>Bhavesh patel</h6><small class="text-muted">Burton</small>
                                    </td>
                                    <td>12</td>
                                </tr>
                                <tr>
                                    <td><span class="round round-primary"><img src="../assets/images/users/4.jpg" alt="user" width="50"></span></td>
                                    <td>
                                        <h6>Nirav Joshi</h6><small class="text-muted">Burton</small>
                                    </td>
                                    <td>10</td>
                                </tr>
                                <tr>
                                    <td><span class="round round-warning"><img src="../assets/images/users/5.jpg" alt="user" width="50"></span></td>
                                    <td>
                                        <h6>Micheal Doe</h6><small class="text-muted">Burton</small>
                                    </td>
                                    <td>12</td>
                                </tr>
                                <tr>
                                    <td><span class="round round-danger"><img src="../assets/images/users/1.jpg" alt="user" width="50"></span></td>
                                    <td>
                                        <h6>Johnathan</h6><small class="text-muted">Burton</small>
                                    </td>
                                    <td>2</td>
                                </tr>
                                <tr>
                                    <td><span class="round round-success">M</span></td>
                                    <td>
                                        <h6>Vishal Doe</h6><small class="text-muted">Burton</small>
                                    </td>
                                    <td>12</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- Column -->
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Right sidebar -->
    <!-- ============================================================== -->
    <!-- .right-sidebar -->
    <div class="right-sidebar">
        <div class="slimscrollright">
            <div class="rpanel-title"> Service Panel <span><i class="ti-close right-side-toggle"></i></span> </div>
            <div class="r-panel-body">
                <ul id="themecolors" class="m-t-20">
                    <li><b>With Light sidebar</b></li>
                    <li><a href="javascript:void(0)" data-theme="default" class="default-theme">1</a></li>
                    <li><a href="javascript:void(0)" data-theme="green" class="green-theme">2</a></li>
                    <li><a href="javascript:void(0)" data-theme="red" class="red-theme">3</a></li>
                    <li><a href="javascript:void(0)" data-theme="blue" class="blue-theme working">4</a></li>
                    <li><a href="javascript:void(0)" data-theme="purple" class="purple-theme">5</a></li>
                    <li><a href="javascript:void(0)" data-theme="megna" class="megna-theme">6</a></li>
                    <li class="d-block m-t-30"><b>With Dark sidebar</b></li>
                    <li><a href="javascript:void(0)" data-theme="default-dark" class="default-dark-theme">7</a></li>
                    <li><a href="javascript:void(0)" data-theme="green-dark" class="green-dark-theme">8</a></li>
                    <li><a href="javascript:void(0)" data-theme="red-dark" class="red-dark-theme">9</a></li>
                    <li><a href="javascript:void(0)" data-theme="blue-dark" class="blue-dark-theme">10</a></li>
                    <li><a href="javascript:void(0)" data-theme="purple-dark" class="purple-dark-theme">11</a></li>
                    <li><a href="javascript:void(0)" data-theme="megna-dark" class="megna-dark-theme ">12</a></li>
                </ul>
                <ul class="m-t-20 chatonline">
                    <li><b>Chat option</b></li>
                    <li>
                        <a href="javascript:void(0)"><img src="assets/images/users/1.jpg" alt="user-img" class="img-circle"> <span>Varun Dhavan <small class="text-success">online</small></span></a>
                    </li>
                    <li>
                        <a href="javascript:void(0)"><img src="assets/images/users/2.jpg" alt="user-img" class="img-circle"> <span>Genelia Deshmukh <small class="text-warning">Away</small></span></a>
                    </li>
                    <li>
                        <a href="javascript:void(0)"><img src="assets/images/users/3.jpg" alt="user-img" class="img-circle"> <span>Ritesh Deshmukh <small class="text-danger">Busy</small></span></a>
                    </li>
                    <li>
                        <a href="javascript:void(0)"><img src="assets/images/users/4.jpg" alt="user-img" class="img-circle"> <span>Arijit Sinh <small class="text-muted">Offline</small></span></a>
                    </li>
                    <li>
                        <a href="javascript:void(0)"><img src="assets/images/users/5.jpg" alt="user-img" class="img-circle"> <span>Govinda Star <small class="text-success">online</small></span></a>
                    </li>
                    <li>
                        <a href="javascript:void(0)"><img src="assets/images/users/6.jpg" alt="user-img" class="img-circle"> <span>John Abraham<small class="text-success">online</small></span></a>
                    </li>
                    <li>
                        <a href="javascript:void(0)"><img src="assets/images/users/7.jpg" alt="user-img" class="img-circle"> <span>Hritik Roshan<small class="text-success">online</small></span></a>
                    </li>
                    <li>
                        <a href="javascript:void(0)"><img src="assets/images/users/8.jpg" alt="user-img" class="img-circle"> <span>Pwandeep rajan <small class="text-success">online</small></span></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Right sidebar -->
    <!-- ============================================================== -->
</div>