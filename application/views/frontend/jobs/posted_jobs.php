<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h1>Job Posted</h1>
                    <a href="<?= base_url('jobs/create'); ?>" class="text-primary " ><i class="mdi mdi-tooltip-edit"></i> Compose new job </a>
                </div>
                <div class="card-body">

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="profiletimeline my-posted-job">
                                <!-- Post Job List Item
                                <?php $this->load->view('frontend/partials/posted_job_item') ?>
                                 End of Job List Item -->
                            </div>
                        </div>
                    </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <ul class="pagination pagination-myjobs-bars">

                                    </ul>
                                </div>
                            </div>
                </div>
            </div>
        </div>
    </div>
</div>
