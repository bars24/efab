
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="custom-container-header mt-0">
                <div class="container">
                    <div class="row">
                        <!--  -->
                        <div class="col-sm-8">
                            <h4 class="card-title font-weight-bold mt-2 text-white">Bid History</h4>

                        </div>

                        <!-- Search Section -->
                        <div class="col-sm-4">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="input-group input-group-sm mb-0 ">
                                        <input type="text" class="form-control border border-white" placeholder="Search Bids">
                                        <span class="input-group-append">
                                                <button class="btn btn-warning text-white">Search</button>
                                        </span>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="card">
                <ul class="list-group list-group-flush pagination-bid-history-container"></ul>
                <div class="pagination pagination-jobs-bars col-12 justify-content-center mb-4"></div>
            </div>
        </div>


    </div>
</div>
