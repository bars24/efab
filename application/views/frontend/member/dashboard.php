<div class="container-fluid">
    <div class="row">
        <div class="col-sm-6">
            <a href="<?php echo base_url('work') ?>">
                <div class="card py-5">
                    <div class="card-body text-center">
                        <h1 class="font-weight-bold">Work</h1>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquam molestiae dicta, magni accusamus a deserunt!</p>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-sm-6">
            <a href="<?php echo base_url('hire') ?>">
                <div class="card py-5">
                    <div class="card-body text-center">
                        <h1 class="font-weight-bold">Hire</h1>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit voluptatibus commodi molestiae dolorum natus ducimus?</p>
                    </div>
                </div>
            </a>
        </div>
    </div>
</div>